import React, { useState } from "react"
import ReactDOM from "react-dom"


import "draft-js/dist/Draft.css"
import "draftail/dist/draftail.css"

import { DraftailEditor, BLOCK_TYPE, INLINE_STYLE } from "draftail"
import { EditorState, ContentState, convertToRaw } from "draft-js"

const initial = JSON.parse(sessionStorage.getItem("draftail:content"))

const onSave = (content) => {
  console.log("saving", content)
  sessionStorage.setItem("draftail:content", JSON.stringify(content))
}

const Editor = () => {

  const textInput = document.getElementById('id_text')
  const [editorState, setEditorState] = useState(
    EditorState.createWithContent(
      ContentState.createFromText(textInput.innerHTML)
    )
  );

  const onChange = (editorState) => {
    setEditorState(editorState)

    const blocks = convertToRaw(editorState.getCurrentContent()).blocks;
    var texts = ""
    for (var i = 0; i < blocks.length; i ++) {
      texts += JSON.stringify(blocks[i]) + "\n"
    }
    textInput.innerHTML = texts
    console.log("currentContent: ", blocks)
  }

  return (
    <DraftailEditor
      editorState={editorState}
      onChange={onChange}
      blockTypes={[
        { type: BLOCK_TYPE.HEADER_TWO },
        { type: BLOCK_TYPE.HEADER_THREE },
        { type: BLOCK_TYPE.UNORDERED_LIST_ITEM },
      ]}
      inlineStyles={[{ type: INLINE_STYLE.BOLD }, { type: INLINE_STYLE.ITALIC }]}
    />
  );
}

ReactDOM.render(<Editor />, document.getElementById('container'))
